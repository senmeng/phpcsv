<?php

/*
 * This file is part of the Csv package.
 *
 * (c) luoqi <luoqi@huxin.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace phpcsv;

class phpcsv
{
    static $header = [
        'Content-Type' => 'application/vnd.ms-excel',
        'Expires' => "gmdate(D, d M Y H:i:s) GMT",
        'Cache-Control' => ' cache, must-revalidate',
        'Pragma' => 'public'
    ];

    const LIMIT = 100;

    public static function add_header() {
        foreach (self::$header as $key => $value) {
            header($key.':'.$value);
        }
        header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
        header('Content-Disposition: attachment;filename="'.date('YmdHis').'.csv"');
    }

    //通过浏览器直接输出
    public static function browser_export($data='') {
        self::add_header();

        ob_flush();
        flush();
        //创建临时存储内存
        $fp = fopen('php://memory','w');
        
        // 计数器
        $cnt = 0;
        foreach ($data as $k=>$v){          
            $cnt ++;
            if (self::LIMIT == $cnt) { //刷新一下输出buffer，防止由于数据过多造成问题
                ob_flush();
                flush();
                $cnt = 0;
            }
           
            if(!empty($v)) {
                foreach ($v as $kk=>$vv){
                    if(preg_match('/^[0-9]*$/',$vv) && strlen($vv)>10){
                        $v[$kk] = $vv . "\t";
                    }
                }
                fputcsv($fp, $v);
            }
        }
        unset($list);
        rewind($fp);
        $content = "";
        while(!feof($fp)){
            $content .= fread($fp,1024);
        }
        fclose($fp);
        $content = iconv('utf-8','gbk',$content);//转成gbk，否则excel打开乱码.
        echo $content;
    }

    //生成CSV文件
    public static function file_export($data='',$file) {
        $date = date('YmdHis');
        $name = $date.'.csv';
        $file_name = $file.$name;
        if(!file_exists($file)) {
            mkdir($file,0777,true);
        }

        // 计数器
        $cnt = 0;
        $limit = 1000;
        ob_flush();
        flush();
        $stime=microtime(true);
        $fp = fopen($file_name, 'w');
        //Windows下使用BOM来标记文本文件的编码方式
        fwrite($fp,chr(239).chr(187).chr(191));

        foreach ($data as $k=>$v) {
            $cnt ++;
            if ($limit == $cnt) { //刷新一下输出buffer，防止由于数据过多造成问题
                ob_flush();
                flush();
                $cnt = 0;
            }

            $v = self::filter_csv($v);
            fputcsv($fp,$v);
        }
        fclose($fp);        

        $etime=microtime(true);//获取程序执行结束的时间
        $total=$etime-$stime;   //计算差值
        return array('filename'=>$date.".csv",'time'=>$total);
    }

    //CSV导出过滤器
    public static function filter_csv($arr){
        foreach ($arr as $kk=>$vv){
            if(is_numeric($vv) && strlen($vv)>10){
                $arr[$kk] = $vv."\t";
            }
        }
        return $arr;
    }
}